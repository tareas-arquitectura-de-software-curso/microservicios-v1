# Arquitecturas - Microservicios

## Sistema de Procesamiento de Comentarios (SPC)

Netflix, Inc. es una empresa estadounidense que provee contenido multimedia en línea (conocido como streaming en el idioma inglés). Actualmente Netflix ha desarrollado contenidos multimedia propios, uno de ellos es Stranger Things que es una serie de fantasía, drama y horror, que en su primera temporada logró 18 nominaciones a los premios Emmy en el año 2016.

Reed Hastings, CEO de Netflix, quiere continuar con el rodaje de la segunda temporada de Stranger Things. A pesar de las nominaciones logradas, le ha pedido al departamento de mercadotecnia de la empresa que realicen un análisis de los comentarios sobre la serie. De esta forma, el departamento de mercadotecnia solicitó al departamento de desarrollo de software un sistema que automatice la encomienda de Reed Hastings.

## Prerrequisitos
- Clonar el repositorio:
   ```shell
   $ git clone https://gitlab.com/tareas-arquitectura-de-software-curso/microservicios.git
   $ cd microservicios
   ```

- Contar con python 3.6 o superior (las pruebas fueron realizadas con la versión 3.6.10). Se recomienda utilizar [pyenv](https://github.com/pyenv/pyenv) como manejador de versiones de python; una vez instalado se pueden seguir los siguientes comandos para instalar la versión deseada de python, esto hay que realizarlo en la raíz del repositorio:
   ```shell
   $ pyenv install 3.6.10
   $ pyenv local 3.6.10
   ```

- Crear un ambiente virtual para manejar las dependencias ejecutando:
   ```shell
   $ python3 -m venv venv
   ```

   o en Windows:
   ```shell
   $ py -3 -m venv venv
   ```

   Esto creará una carpeta llamada "venv" que representa nuestro ambiente virtual y donde instalaremos todas las dependencias.

- Activamos el ambiente virtual:
   ```shell
   $ source venv/bin/activate
   ```

   o en Windows:
   ```shell
   $ venv\Scripts\activate
   ```

- Instalamos las dependencias del sistema ejecutando:
   ```shell
   (venv)$ pip3 install -r requirements.txt 
   ```

   Los paquetes que se instalarán son los siguientes:

   Paquete | Versión | Descripción
   --------|---------|------------
   Flask   | 1.1.1   | Micro framework de desarrollo
   requests| 2.23.0  | API interna utilizada en Flask para trabajar con las peticiones hacia el servidor

   *__Nota__: También puedes instalar estos prerrequisitos manualmente ejecutando los siguientes comandos:*   
   > pip3 install Flask== 1.1.1  
   > pip3 install requests==2.23.0

## Ejecución

Una vez instalados los prerrequisitos es momento de ejecutar el sistema siguiendo los siguientes pasos:  
1. Ejecutar el servicio que obtiene la información:
   ```shell
   (venv)$ python3 servicios/sv_information.py
   ```
1. Ejecutar el GUI:  
   ```shell
   (venv)$ python3 gui.py
   ```
1. Abrir el navegador
1. Acceder a la url del sistema:
   > http://localhost:8000/

## Versión

2.0.0 - Febrero 2020

## Autores

* **Perla Velasco**
* **Yonathan Martínez**
